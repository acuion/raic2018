﻿#include "MyActionWrapper.h"
#include "MyWorldSim.h"
#include "MyRobotRoleDeterminer.h"
#include "MyPersistence.h"
#include "MyHelper.h"

MyActionWrapper::MyActionWrapper(const Robot & me, const Rules & rules, const Game & game, Action & action)
    : me(me)
    , rules(rules)
    , game(game)
    , inRoleNumber(MyRobotRoleDeterminer::whoAmI(me.id).first)
    , mainInThisRole(inRoleNumber == 0)
    , thisPhyRobotStill(MyPhyRobot(me, rules, MyPhyRobotAction::empty))
    , action(action) {
}

void MyActionWrapper::nitroTrick() {
    if (me.nitro_amount == 0) {
        return;
    }

    const auto phyBall = MyPhyBall(game.ball, rules);
    auto nitroVel = nitroVelo(phyBall, thisPhyRobotStill);

    if (!nitroVel.isZero()) {
        nitroVel.fillActionVelocity(action);
        action.use_nitro = true;
    }
}

void MyActionWrapper::runJumpTo(const Vector & target, JumpTargetAlgorithm targetAlgo, double jumpspeed) const {
    if (jumpspeed == -1) {
        jumpspeed = rules.ROBOT_MAX_JUMP_SPEED;
    }

    auto correctedTarget = target;

    const double rx = rules.arena.width / 2 - rules.arena.bottom_radius * 0.7;
    const double lx = -rules.arena.width / 2 + rules.arena.bottom_radius * 0.7;
    if (correctedTarget.x > rx) {
        correctedTarget.x = rx;
    }
    if (correctedTarget.x < lx) {
        correctedTarget.x = lx;
    }

    ((correctedTarget - thisPhyRobotStill.position).norm() * rules.ROBOT_ACCELERATION).fillActionVelocity(action);

    if (correctedTarget.y > 0) {
        if (thisPhyRobotStill.jumpWillHitInTicks(correctedTarget, jumpspeed, me.nitro_amount)) {
            action.jump_speed = jumpspeed;
            MyPersistence::storeJumpTargetAlgorithm(me.id, targetAlgo);
        }
    }
}

MyVisHelper MyActionWrapper::wantVis() const {
    return visio;
}

InterceptionResult MyActionWrapper::interceptScore(MyWorldSim sima, int ticksPassed, const Vector& originalTarget,
                                                   double jumpSpeed, const std::function<int(const MyPhyBall&, const MyPhyRobot&)>& valueFunc, int ticksToSim, double simaPrecision,
                                                   RepositionFunction respositionFunction, JumpTargetAlgorithm jumpTargetAlgorithm) const {
    const auto ourGatesZ = -rules.arena.depth / 2;
    const auto theirGatesZ = rules.arena.depth / 2;
    const auto theirGoalLine = theirGatesZ + rules.BALL_RADIUS;
    const auto ourGoalLine = ourGatesZ - rules.BALL_RADIUS;

    bool reachedTarget = false;
    bool touchedBall = false;
    int sumDiff = 10000;
    int timeDiff = 0;
    for (int tdc = 0; tdc < std::min(12, ticksPassed); ++tdc) {
        auto simaToGetDiff = MyWorldSim(sima.getBall(), { thisPhyRobotStill }, rules, false);
        auto& currentRobot = simaToGetDiff.getRobots().back();
        simaToGetDiff.setPrecision(0.08);
        for (int i = 0; i < ticksToSim; ++i) {
            {
                auto runTo = originalTarget;
                if (i < tdc) {
                    runTo = respositionFunction(currentRobot.position);
                }
                currentRobot.action.target_velocity = (runTo - currentRobot.position).noY().norm() * rules.ROBOT_MAX_GROUND_SPEED;
                if (currentRobot.touch && runTo.y > 0 && currentRobot.jumpWillHitInTicks(runTo, jumpSpeed, me.nitro_amount)) {
                    currentRobot.action.jump_speed = jumpSpeed;
                }
            }

            if ((currentRobot.position - originalTarget).len() < rules.ROBOT_RADIUS * 1.4) {
                reachedTarget = true;
                const int curd = abs(i - ticksPassed);
                if (curd < sumDiff) {
                    timeDiff = tdc;
                    sumDiff = curd;
                }
            }

            simaToGetDiff.tick();
        }

        if (tdc == 0 && !reachedTarget) {
            return InterceptionResult(_MINUS_INF + 1, timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
        }
    }

    sima.setPrecision(simaPrecision);
    sima.getRobots().push_back(thisPhyRobotStill);
    auto& currentRobot = sima.getRobots().back();
    Vector target = originalTarget;
    bool inFly = false;
    for (int i = 0; i < ticksToSim; ++i) {
        if (i == ticksPassed + 5 && !touchedBall) {
            return InterceptionResult(_MINUS_INF + 2, timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
        }
        if (touchedBall) {
            target = sima.getBall().position;
        }
        {
            auto runTo = target;
            if (i < timeDiff) {
                runTo = respositionFunction(currentRobot.position);
            }
            if (inFly && currentRobot.nitro != 0) {
                auto nitroVel = nitroVelo(sima.getBall(), currentRobot);
                if (!nitroVel.isZero()) {
                    currentRobot.action.target_velocity = nitroVel;
                    currentRobot.action.use_nitro = true;
                }
            }
            if (currentRobot.touch) {
                currentRobot.action.target_velocity = (runTo - currentRobot.position).noY().norm() * rules.ROBOT_MAX_GROUND_SPEED;
                if (runTo.y > 0 && currentRobot.jumpWillHitInTicks(runTo, jumpSpeed, me.nitro_amount)) {
                    currentRobot.action.jump_speed = jumpSpeed;
                    inFly = true;
                }
            }
        }

        sima.tick();

        if (currentRobot.collidedWithBallThisTurn && !touchedBall) {
            touchedBall = true;
        }

        if (sima.getBall().position.z > theirGoalLine) { // goal+
            if (!touchedBall) {
                return InterceptionResult(_MINUS_INF + 2, timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
            }
            return InterceptionResult(_PLUS_INF, timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
        }

        if (sima.getBall().position.z < ourGoalLine) { // goal-
            return InterceptionResult(_MINUS_INF + 3, timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
        }
    }

    if (!touchedBall) {
        return InterceptionResult(_MINUS_INF + 2, timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
    }

    auto ret = InterceptionResult(valueFunc(sima.getBall(), currentRobot), timeDiff, originalTarget, jumpSpeed, jumpTargetAlgorithm);
    ret.interBallPos = sima.getBall().position;
    return ret;
}

Vector MyActionWrapper::nitroVelo(const MyPhyBall& phyBall, const MyPhyRobot& robot) const {
    if (robot.nitro == 0) {
        return Vector::zero;
    }

    auto jtf = MyPersistence::getJumpTarget(me.id);
    auto jt = jtf(phyBall.position);
    if (jt.isZero()) {
        return Vector::zero;
    }

    auto nitroSima = MyWorldSim(phyBall, { robot }, rules);
    nitroSima.setPrecision(0.01);

    const auto target = jt - robot.position;
    auto targetVelo = target.norm() * rules.MAX_ENTITY_SPEED;

    bool ballTouched = false;
    auto& nitroLastRobot = nitroSima.getRobots().back();
    for (int i = 0; i < 16; ++i) {
        nitroLastRobot.action.target_velocity = targetVelo;
        nitroLastRobot.action.use_nitro = true;
        nitroSima.tick();

        if (nitroLastRobot.collidedWithBallThisTurn) {
            ballTouched = true;
            break;
        }
    }

    if (ballTouched) {
        return targetVelo;
    }
    return Vector::zero;
}
