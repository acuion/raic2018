﻿#pragma once

#include "MyActionWrapper.h"
#include "MyWorldSim.h"
class MyWorldSim;
class MyPhyRobot;
using namespace model;

class MyAttacker : public MyActionWrapper {
public:
	MyAttacker(const Robot& me, const Rules& rules, const Game& game, Action& action);

	void act() override;
    Vector getReposPoint(const Vector& landingPt, const Vector& robotPosition);
private:
    int waitDistAdd = 0;
};
