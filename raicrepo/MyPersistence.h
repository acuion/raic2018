﻿#pragma once

#include "MyMath.h"
#include <map>
#include <functional>

using JumpTargetAlgorithm = std::function<Vector(const Vector&)>;

class MyPersistence {
public:
    static void storeJumpTargetAlgorithm(int id, JumpTargetAlgorithm jumpTargetAlgorithm) {
        jumpTargets[id] = jumpTargetAlgorithm;
    }

    static JumpTargetAlgorithm getJumpTarget(int id) {
        if (jumpTargets.count(id) == 0) {
            return [](const Vector&) { return Vector::zero; };
        }
        return jumpTargets[id];
    }

    static void removeJumpTarget(int id) {
        jumpTargets.erase(id);
    }

private:
    static std::map<int, JumpTargetAlgorithm> jumpTargets;
};
