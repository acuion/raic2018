#include "MyStrategy.h"
#include "MyRobotRoleDeterminer.h"
#include "MyActionWrapper.h"
#include "MyAttacker.h"
#include "MyDefender.h"
#include "MyTimeManager.h"

using namespace model;

MyStrategy::MyStrategy() {
}

void MyStrategy::act(const Robot& me, const Rules& rules, const Game& game, Action& action) {
    MyTimeManager::start();
#ifdef LOCALRUN
    if (game.current_tick < 0) {
        MyTimeManager::stop();
        return;
    }
#endif
    if (abs(game.ball.z) > rules.arena.depth / 2 + rules.BALL_RADIUS) {
        MyTimeManager::stop();
        return;
    }

    if (lastTick != game.current_tick) {
        lastTick = game.current_tick;
        MyRobotRoleDeterminer::clear();
        wantRender.reset();
    }

	const auto robotRole = MyRobotRoleDeterminer::determine(me, rules, game);
    
	MyActionWrapper* robotActionModel = nullptr;

	switch (robotRole) {
	case RobotRole::ATTACKER:
		robotActionModel = new MyAttacker(me, rules, game, action);
		break;
	case RobotRole::DEFENDER:
		robotActionModel = new MyDefender(me, rules, game, action);
		break;
	}

	robotActionModel->act();
	wantRender.add(robotActionModel->wantVis());

	delete robotActionModel;
    MyTimeManager::stop();
}

std::string MyStrategy::custom_rendering() {
#ifndef LOCALRUN
    return "";
#endif
	return wantRender.asJson();
}
