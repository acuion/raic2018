﻿#include "MyDefender.h"
#include "MyMath.h"
#include "MyWorldSim.h"
#include "MyHelper.h"
#include "MyRobotRoleDeterminer.h"

MyDefender::MyDefender(const Robot & me, const Rules & rules, const Game & game, Action & action)
    : MyActionWrapper(me, rules, game, action) {
}

void MyDefender::act() {
    if (!me.touch) {
        nitroTrick();
        return;
    }

    double nestedPrecision = 0.4;
    int tickToNestedSim = 52;

    const auto ourGatesZ = -rules.arena.depth / 2;
    const auto interceptY = rules.arena.goal_height;
    auto interceptZ = -13;
    auto annoyZ = std::max(me.z + rules.BALL_RADIUS, ourGatesZ + rules.arena.goal_depth / 2);
    if (!mainInThisRole) {
        tickToNestedSim = 30;
        nestedPrecision = 0.3;
        annoyZ = std::max(annoyZ, ourGatesZ + rules.arena.goal_depth);
        interceptZ = -25;
    }
    const auto gatesXL = -rules.arena.goal_width / 2;
    const auto gatesXR = rules.arena.goal_width / 2;

    auto robotsToSim = Helper::getEnemyRobotsPhy(game, rules);
    for (auto& x : game.robots) {
        if (x.is_teammate && x.id != me.id) {
            robotsToSim.emplace_back(x, rules, MyPhyRobotAction::empty);
            robotsToSim.back().is_teammate = false; // sorry, bro
        }
    }
    const auto phyBall = MyPhyBall(game.ball, rules);
    const auto sourceSima = MyWorldSim(phyBall, robotsToSim, rules);
    auto finalBallSima = sourceSima;
    finalBallSima.setPrecision(nestedPrecision);
    if (thisPhyRobotStill.velocity.len() < 2) {
        finalBallSima.getRobots().push_back(thisPhyRobotStill);
    }
    for (int i = 0; i < tickToNestedSim; ++i) {
        finalBallSima.tick();
        if (finalBallSima.getBall().position.z < ourGatesZ + rules.BALL_RADIUS) {
            finalBallSima.getBall().position.z = ourGatesZ - rules.arena.goal_depth;
            break;
        }
    }

    auto sima = sourceSima;
    if (thisPhyRobotStill.velocity.len() < 2) {
        sima.getRobots().push_back(thisPhyRobotStill);
    }
    sima.setPrecision(0.40);

    double discount = 0.95;
    std::vector<InterceptionCandidate> candidates;
    std::vector<InterceptionResult> results;
    Vector posToRebase;

    bool intercept = false;
    if (game.ball.z < 0 || game.ball.velocity_z < -20) {
        bool goalIsSoon = false;
        for (int i = 0; i < 1.7 * tickToNestedSim; ++i) {
            if (sima.getBall().position.y <= interceptY && i < tickToNestedSim
                && sima.getBall().position.z < interceptZ) {
                const double angleToRobot = sima.getBall().position.angle2DTo(thisPhyRobotStill.position);

                const auto ballRadius = rules.BALL_RADIUS;
                {
                    const JumpTargetAlgorithm algo = [=](const Vector& ballPos) {
                        return Vector(ballPos.x + ballRadius * cos(angleToRobot),
                            ballPos.y,
                            ballPos.z + ballRadius * sin(angleToRobot));
                    };
                    candidates.emplace_back(algo(sima.getBall().position), sima.getBall().position, rules.ROBOT_MAX_JUMP_SPEED, algo, i);
                }
                {
                    const JumpTargetAlgorithm algo = [=](const Vector& ballPos) {
                        return Vector(ballPos.x + ballRadius * cos(angleToRobot) * 0.8,
                            ballPos.y - ballRadius * 0.7,
                            ballPos.z + ballRadius * sin(angleToRobot) * 0.8);
                    };
                    candidates.emplace_back(algo(sima.getBall().position), sima.getBall().position, rules.ROBOT_MAX_JUMP_SPEED, algo, i);
                }
            }

            if (sima.getBall().position.z < ourGatesZ - rules.BALL_RADIUS) {
                // goal, run the best interception
                intercept = true;
                if (i < 20) {
                    discount = 0.8;
                    posToRebase = sima.getBall().position;
                    goalIsSoon = true;
                }
                break;
            }

            if (sima.getBall().position.z < annoyZ
                && sima.getBall().position.x > gatesXL
                && sima.getBall().position.x < gatesXR) {
                intercept = true;
            }

            if (i == 20) {
                posToRebase = sima.getBall().position;
            }

            sima.tick();
        }

        if (intercept) {
            const RepositionFunction valueRepositionFunction = [&](const Vector& robotPos) {
                return getReposPointAtck(phyBall.position, posToRebase, robotPos);
            };

            candidates.emplace_back(thisPhyRobotStill.position + Vector(0, 0.1, 0), phyBall.position, rules.ROBOT_MAX_JUMP_SPEED, [=](const Vector& ballPos) { return ballPos; }, 1);
            for (auto& x : candidates) {
                if (x.interceptPoint.z > x.ballPos.z) {
                    continue;
                }
                const auto valueFunc = [&](const MyPhyBall& resultBall, const MyPhyRobot& robot) {
                    const auto getDistToGates = [&](const Vector& pos) {
                        if (pos.x >= gatesXL && pos.x <= gatesXR) {
                            return pos.z - ourGatesZ;
                        }
                        if (pos.x < gatesXL) {
                            return pos.dist2dTo(Vector(gatesXL, 0, ourGatesZ));
                        }
                        return pos.dist2dTo(Vector(gatesXR, 0, ourGatesZ));
                    };

                    return 100 * (getDistToGates(resultBall.position) - getDistToGates(finalBallSima.getBall().position))
                        - 100 * getDistToGates(robot.position);
                };
                auto interceptionResult = interceptScore(sourceSima, x.ticksPassed, x.interceptPoint,
                    x.jumpSpeed, valueFunc, tickToNestedSim, nestedPrecision, valueRepositionFunction, x.algo);
                interceptionResult.ballPos = x.ballPos;
                interceptionResult.ticksPassed = x.ticksPassed;
                if (interceptionResult.score >= 0) {
                    results.push_back(interceptionResult);
                }
            }
        }

        if (!results.empty()) {
            std::vector<double> precompDiscount;
            double disc = 1;
            for (int i = 0; i < tickToNestedSim; ++i) {
                precompDiscount.push_back(disc);
                disc *= discount;
            }
            const auto maxInterception = *std::max_element(results.begin(), results.end(), [&](const InterceptionResult& a, const InterceptionResult& b) {
                return a.score * precompDiscount[a.ticksPassed] < b.score * precompDiscount[b.ticksPassed];
            });
            if (maxInterception.timeDiff < 1) {
                runJumpTo(maxInterception.point, maxInterception.algo, maxInterception.jumpSpeed);
                return;
            }

            runJumpTo(getReposPointAtck(phyBall.position, maxInterception.ballPos, thisPhyRobotStill.position));
            return;
        } else if (goalIsSoon) {
            runJumpTo(phyBall.position, [](const Vector& b) {return b; });
            if (game.ball.y > 6 && me.velocity_z > 0) {
                action.jump_speed = rules.ROBOT_MAX_JUMP_SPEED;
                action.use_nitro = true;
            }
            return;
        }
        else {
            runJumpTo(getReposPoint(phyBall.position, posToRebase, thisPhyRobotStill.position));
            return;
        }
    }

    // goto nitro!
    {
        if (me.nitro_amount != rules.MAX_NITRO_AMOUNT) {
            const NitroPack* best = nullptr;
            int distance = 1e9;
            for (auto &p : game.nitro_packs) {
                if (p.alive) {
                    const auto currd = Vector(p).dist2dTo(thisPhyRobotStill.position);
                    if (distance > currd) {
                        distance = currd;
                        best = &p;
                    }
                }
            }
            if (best != nullptr && distance < rules.arena.depth / 2) {
                runJumpTo(Vector(*best).noY());
                return;
            }
        }
    }

    // goto wait point
    runJumpTo(Vector(0, 0, -rules.arena.depth / 2 - 4));
}

Vector MyDefender::getReposPoint(const Vector& currentBallPos, const Vector & futureBallPos, const Vector & robotPosition) {
    const auto ourGatesZ = -rules.arena.depth / 2;
    const auto gatesXL = -rules.arena.goal_width / 2;
    const auto gatesXR = rules.arena.goal_width / 2;
    Vector target;

    double wpX = futureBallPos.x;
    double zpos = ourGatesZ - 4;
    if (!mainInThisRole) {
        wpX *= -1;
        zpos = ourGatesZ - 1;
    }
    const auto waitPoint = Vector(std::clamp(wpX, gatesXL + 8, gatesXR - 8), 0, zpos);
    if (game.ball.z > me.z || game.ball.y > rules.ROBOT_RADIUS * 3) {
        target = waitPoint;
    }
    else {
        const auto angleToRobot = currentBallPos.angle2DTo(robotPosition);
        const auto p1 = Vector(currentBallPos.x + cos(angleToRobot + _PI / 4) * rules.ROBOT_RADIUS * 3,
            0,
            currentBallPos.z + sin(angleToRobot + _PI / 4) * rules.ROBOT_RADIUS * 3);
        const auto p2 = Vector(currentBallPos.x + cos(angleToRobot - _PI / 4) * rules.ROBOT_RADIUS * 3,
            0,
            currentBallPos.z + sin(angleToRobot - _PI / 4) * rules.ROBOT_RADIUS * 3);
        if (waitPoint.dist2dTo(p1) < waitPoint.dist2dTo(p2)) {
            target = p1;
        }
        else {
            target = p2;
        }
    }
    return target;
}

Vector MyDefender::getReposPointAtck(const Vector& currentBallPos, const Vector & futureBallPos, const Vector & robotPosition) {
    const auto ourGatesZ = -rules.arena.depth / 2;
    const auto gatesXL = -rules.arena.goal_width / 2;
    const auto gatesXR = rules.arena.goal_width / 2;
    const int waitDistance = 6;
    const auto ourGatesPt = Vector(0, 0, -rules.arena.depth / 2 - 7);
    const auto angleBallToAlly = ourGatesPt.angle2DTo(futureBallPos);

    Vector target = Vector(futureBallPos.x - cos(angleBallToAlly) * rules.ROBOT_RADIUS * waitDistance,
        0,
        futureBallPos.z - sin(angleBallToAlly) * rules.ROBOT_RADIUS * waitDistance);

    if (game.ball.z > me.z || game.ball.y > rules.ROBOT_RADIUS * 3) {
        return target;
    }
    else {
        const auto angleToRobot = currentBallPos.angle2DTo(robotPosition);
        const auto p1 = Vector(currentBallPos.x + cos(angleToRobot + _PI / 4) * rules.ROBOT_RADIUS * 3,
            0,
            currentBallPos.z + sin(angleToRobot + _PI / 4) * rules.ROBOT_RADIUS * 3);
        const auto p2 = Vector(currentBallPos.x + cos(angleToRobot - _PI / 4) * rules.ROBOT_RADIUS * 3,
            0,
            currentBallPos.z + sin(angleToRobot - _PI / 4) * rules.ROBOT_RADIUS * 3);
        if (target.dist2dTo(p1) < target.dist2dTo(p2)) {
            target = p1;
        }
        else {
            target = p2;
        }
    }
    return target;
}