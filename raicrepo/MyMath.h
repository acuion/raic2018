﻿#pragma once

#include <cmath>
#include <algorithm>
#include "Strategy.h"
#include <iostream>
using namespace model;

const double _PI = 3.14159265359;
const int _MINUS_INF = -1000000;
const int _PLUS_INF = 1000000;

class Vector {
public:
    Vector() {
        x = y = z = 0;
    }

	Vector(double x, double y, double z) {
		this->x = x;
		this->y = y;
		this->z = z;
	}

	explicit Vector(const Robot& r) {
		this->x = r.x;
		this->y = r.y;
		this->z = r.z;
	}

	explicit Vector(const Ball& b) {
		this->x = b.x;
		this->y = b.y;
		this->z = b.z;
	}

    explicit Vector(const NitroPack& b) {
        this->x = b.x;
        this->y = b.y;
        this->z = b.z;
    }

	double len() const {
		return sqrt(x * x + y * y + z * z);
	}

    double sqLen() const {
        return x * x + y * y + z * z;
    }

	Vector norm() const {
		const double cf = len();
		return {x / cf, y / cf, z / cf};
	}

	Vector operator* (double d) const {
		return {x * d, y * d, z * d};
	}

	Vector operator- (const Vector& vec) const {
		return { x - vec.x, y - vec.y, z - vec.z };
	}

	void operator-= (const Vector& vec) {
        x -= vec.x;
        y -= vec.y;
        z -= vec.z;
    }

    void operator+= (const Vector& vec) {
        x += vec.x;
        y += vec.y;
        z += vec.z;
    }

	void operator/= (double d) {
		x /= d;
		y /= d;
		z /= d;
	}

	Vector operator+ (const Vector& vec) const {
		return { x + vec.x, y + vec.y, z + vec.z };
	}

	bool operator != (const Vector& vec) const {
		return vec.x != x || vec.y != y || vec.z != z;
	}

	bool operator == (const Vector& vec) const {
		return !(*this != vec);
	}

	bool operator< (const Vector& vec) const {
		return len() < vec.len();
	}

	Vector noY() const {
		return{ x, 0, z };
	}

    Vector abs() const {
        return { fabs(x), fabs(y), fabs(z) };
	}

    Vector absx() const {
        return { fabs(x), y, z };
    }

    double dot(const Vector& vec) const {
        return x * vec.x + y * vec.y + z * vec.z;
    }

	bool isZero() const {
		return x == 0 && y == 0 && z == 0;
	}

	double dist2dTo(const Vector& vec) const {
		return (Vector(x, 0, z) - Vector(vec.x, 0, vec.z)).len();
	}

	void fillActionVelocity(Action & action) const {
		action.target_velocity_x = x;
		action.target_velocity_y = y;
		action.target_velocity_z = z;
	}

	double angle2DTo(const Vector& vec) const {
		return atan2(vec.z - z, vec.x - x);
	}

	bool lessThanZxy(const Vector& vec) const {
		if (z != vec.z) {
			return z < vec.z;
		}
		if (x != vec.x) {
			return x < vec.x;
		}
		return y < vec.y;
	}

    Vector clamp(double len) const {
		if (this->sqLen() <= len * len) {
			return *this;
		}
        return norm() * len;
    }

    void selfclamp(double len) {
        if (sqLen() <= len * len) {
            return;
        }
        const double length = this->len();
        x = x / length * len;
        y = y / length * len;
        z = z / length * len;
    }

	double x, y, z;

    static Vector zero;
};
