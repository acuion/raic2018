#include "MyWorldSim.h"
#include "MyMath.h"
#include <algorithm>

MyPhyRobotAction MyPhyRobotAction::empty = MyPhyRobotAction(Vector(0, 0, 0), 0);

MyWorldSim::MyWorldSim(const MyPhyBall & ball,
    const std::vector<MyPhyRobot>& robots,
    const Rules& rules,
    bool ballEnabled)
    : rules(rules)
    , ballEnabled(ballEnabled)
    , ball(ball)
    , robots(robots)
    , useMicroticks(rules.MICROTICKS_PER_TICK) {
}

void MyWorldSim::step(double delta_time) {
    for (auto& robot : robots) {
        if (robot.touch) {
            auto target_velocity = robot.action.target_velocity.clamp(rules.ROBOT_MAX_GROUND_SPEED);
            target_velocity -= robot.touch_normal * robot.touch_normal.dot(target_velocity);
            auto target_velocity_change = target_velocity - robot.velocity;
            const auto tvc_len = target_velocity_change.sqLen();
            if (tvc_len > 0) {
                auto acceleration = rules.ROBOT_ACCELERATION * std::max(0.0, robot.touch_normal.y);
                const auto adt = acceleration * delta_time;
                if (tvc_len > adt * adt) {
                    target_velocity_change = target_velocity_change.norm() * adt;
                }
                robot.velocity += target_velocity_change;
            }
        }

        if (robot.action.use_nitro) {
            const auto target_velocity_change = (robot.action.target_velocity - robot.velocity).clamp(robot.nitro * rules.NITRO_POINT_VELOCITY_CHANGE);
            const auto tvcLen = target_velocity_change.sqLen();
            if (tvcLen > 0) {
                const auto acceleration = target_velocity_change.norm() * rules.ROBOT_NITRO_ACCELERATION;
                const auto velocity_change = (acceleration * delta_time).clamp(sqrt(tvcLen));
                robot.velocity += velocity_change;
                robot.nitro -= velocity_change.len() / rules.NITRO_POINT_VELOCITY_CHANGE;
            }
        }

        move(robot, delta_time);
        robot.radius = rules.ROBOT_MIN_RADIUS + (rules.ROBOT_MAX_RADIUS - rules.ROBOT_MIN_RADIUS)
            * robot.action.jump_speed / rules.ROBOT_MAX_JUMP_SPEED;
        robot.radius_change_speed = robot.action.jump_speed;
    }

    if (ballEnabled) {
        move(ball, delta_time);
    }

    for (int i = 0; i < robots.size(); ++i) {
        for (int j = 0; j < i; ++j) {
            collide_entities(robots[i], robots[j]);
        }
    }

    for (auto& robot : robots) {
        if (ballEnabled) {
            collide_entities(robot, ball);
        }
        auto collision_normal = collide_with_arena(robot);
        if (collision_normal.isZero()) {
            robot.touch = false;
        }
        else {
            robot.touch = true;
            robot.touch_normal = collision_normal;
        }
    }

    if (ballEnabled) {
        collide_with_arena(ball);
    }
    // scores

    // nitro
}

void MyWorldSim::tick() {
    for (auto& robot : robots) {
        robot.collidedWithBallThisTurn = false;
        if (!robot.is_teammate) {
            robot.action = MyPhyRobotAction(robot.velocity, 0);
        }
    }

    const double delta = 1.0 / rules.TICKS_PER_SECOND;
    for (int i = 0; i < useMicroticks; ++i) {
        step(delta / useMicroticks);
    }

    for (auto& robot : robots) {
        robot.action = MyPhyRobotAction::empty;
    }
}

MyPhyBall & MyWorldSim::getBall() {
    return ball;
}

std::vector<MyPhyRobot>& MyWorldSim::getRobots() {
    return robots;
}

void MyWorldSim::setPrecision(double p) {
    useMicroticks = rules.MICROTICKS_PER_TICK * p;
}

Vector MyWorldSim::collide_with_arena(MyPhyObject & e) {
    auto[distance, normal] = dan_to_arena(e.position);
    auto penetration = e.radius - distance;
    if (penetration > 0) {
        e.position += normal * penetration;
        auto velocity = e.velocity.dot(normal) - e.radius_change_speed;
        if (velocity < 0) {
            e.velocity -= normal * ((1 + e.arena_e) * velocity);
            return normal;
        }
    }
    return Vector::zero;
}

void MyWorldSim::move(MyPhyObject & e, double delta_time) {
    e.velocity.selfclamp(rules.MAX_ENTITY_SPEED);
    e.position += e.velocity * delta_time;
    e.position.y -= rules.GRAVITY * delta_time * delta_time / 2;
    e.velocity.y -= rules.GRAVITY * delta_time;

}

void MyWorldSim::collide_entities(MyPhyObject & a, MyPhyObject & b) {
    auto delta_position = b.position - a.position;
    auto distance = delta_position.len();
    auto penetration = a.radius + b.radius - distance;
    if (penetration > 0) {
        if (b.id == -1) {
            a.collidedWithBallThisTurn = true;
        }
        auto k_a = a.invMass / (a.invMass + b.invMass);
        auto k_b = b.invMass / (a.invMass + b.invMass);
        auto normal = delta_position.norm();
        a.position -= normal * penetration * k_a;
        b.position += normal * penetration * k_b;
        auto delta_velocity = (b.velocity - a.velocity).dot(normal)
            - b.radius_change_speed - a.radius_change_speed;
        if (delta_velocity < 0) {
            auto impulse = normal * (1 + (rules.MIN_HIT_E + rules.MAX_HIT_E) / 2) * delta_velocity;
            a.velocity += impulse * k_a;
            b.velocity -= impulse * k_b;
        }
    }
}

MyPhyObject::MyPhyObject(const Robot & robot, const Rules & rules, int id)
    : velocity(Vector(robot.velocity_x, robot.velocity_y, robot.velocity_z))
    , position(robot)
    , mass(rules.ROBOT_MASS)
    , invMass(1.0 / mass)
    , radius(rules.ROBOT_RADIUS)
    , radius_change_speed(0)
    , arena_e(rules.ROBOT_ARENA_E)
    , id(id)
    , collidedWithBallThisTurn(false)
    , rules(rules) {
}

MyPhyObject::MyPhyObject(const Ball & ball, const Rules & rules)
    : velocity(Vector(ball.velocity_x, ball.velocity_y, ball.velocity_z))
    , position(ball)
    , mass(rules.BALL_MASS)
    , invMass(1.0 / mass)
    , radius(rules.BALL_RADIUS)
    , radius_change_speed(0)
    , arena_e(rules.BALL_ARENA_E)
    , id(-1)
    , collidedWithBallThisTurn(true)
    , rules(rules) {
}

MyPhyRobot::MyPhyRobot(const Robot & robot, const Rules & rules, MyPhyRobotAction action)
    : MyPhyObject(robot, rules, robot.id)
    , touch(robot.touch)
    , touch_normal(Vector(robot.touch_normal_x, robot.touch_normal_y, robot.touch_normal_z))
    , radius(robot.radius)
    , is_teammate(robot.is_teammate)
    , nitro(robot.nitro_amount)
    , action(action) {
}

bool MyPhyRobot::jumpWillHitInTicks(const Vector& target, double jumpspeed, double nitoAmount) const {
    const double upct = -jumpspeed / -rules.GRAVITY;
    if (target.y - 1 > -rules.GRAVITY / 2 * upct * upct + jumpspeed * upct) {
        return false;
    }

    const int notJumpThreshold = 4;
    const auto probe = [&](MyPhyRobot phyQuad) {
        const double tx = (target.x - phyQuad.position.x) / phyQuad.velocity.x;
        const double tz = (target.z - phyQuad.position.z) / phyQuad.velocity.z;

        const auto f = [&](double t) {
            const auto a = (phyQuad.position.x + t * phyQuad.velocity.x - target.x);
            const auto b = (-rules.GRAVITY / 2 * t * t + jumpspeed * t - target.y);
            const auto c = (phyQuad.position.z + t * phyQuad.velocity.z - target.z);
            return a * a + b * b + c * c;
        };

        return f(tz) <= 1 || f(tx) <= 1;
    };

    if (velocity.sqLen() > (rules.ROBOT_MAX_GROUND_SPEED * 0.6) * (rules.ROBOT_MAX_GROUND_SPEED * 0.6)) {
        auto furtherRobot = *this;
        furtherRobot.position += furtherRobot.velocity * (1.0 / rules.TICKS_PER_SECOND) * notJumpThreshold;
        if (probe(furtherRobot)) {
            return false;
        }
    }

    return probe(*this);
}

MyPhyBall::MyPhyBall(const Ball & ball, const Rules & rules)
    : MyPhyObject(ball, rules) {
}

MyPhyRobotAction::MyPhyRobotAction(Vector targetVelocity, double targetJumpSpeed, bool use_nitro)
    : target_velocity(targetVelocity)
    , jump_speed(targetJumpSpeed)
    , use_nitro(use_nitro) {
}

MyPhyRobotAction::MyPhyRobotAction(const Action & action)
    : target_velocity(action.target_velocity_x, action.target_velocity_y, action.target_velocity_z)
    , jump_speed(action.jump_speed)
    , use_nitro(action.use_nitro) {
}

// GEOM!

std::pair<double, Vector> MyWorldSim::dan_to_plane(const Vector& point, const Vector& point_on_plane, const Vector& plane_normal) {
    return{
        (point - point_on_plane).dot(plane_normal), //distance
        plane_normal //normal
    };
}

std::pair<double, Vector> MyWorldSim::dan_to_sphere_inner(const Vector& point, const Vector& sphere_center, double sphere_radius) {
    return{
        sphere_radius - (point - sphere_center).len(),
        (sphere_center - point).norm()
    };
}

std::pair<double, Vector> MyWorldSim::dan_to_sphere_outer(const Vector& point, const Vector& sphere_center, double sphere_radius) {
    return{
        (point - sphere_center).len() - sphere_radius,
        (point - sphere_center).norm()
    };
}

std::pair<double, Vector> MyWorldSim::dan_to_arena_quarter(const Vector& point) {
    const bool innerCage = abs(point.x) < rules.arena.width / 2 - rules.arena.bottom_radius
        && point.y > rules.arena.bottom_radius
        && point.y < rules.arena.height - rules.arena.top_radius
        && abs(point.z) < rules.arena.depth / 2 - rules.arena.bottom_radius;

    if (innerCage) {
        return { 1000000, point };
    }

    const bool onlyGround = point.y < rules.arena.height / 2
        && abs(point.x) < rules.arena.width / 2 - rules.arena.bottom_radius
        && abs(point.z) < rules.arena.depth / 2 - rules.arena.bottom_radius;

    // Ground
    auto dan = dan_to_plane(point, Vector(0, 0, 0), Vector(0, 1, 0));

    if (onlyGround) {
        return dan;
    }

    const bool onlyCeiling = point.y > rules.arena.height / 2
        && abs(point.x) < rules.arena.width / 2 - rules.arena.top_radius
        && abs(point.z) < rules.arena.depth / 2 - rules.arena.top_radius;

    // Ceiling
    dan = min(dan, dan_to_plane(point, Vector(0, rules.arena.height, 0), Vector(0, -1, 0)));

    if (onlyCeiling) {
        return dan;
    }

    // Side x
    dan = min(dan, dan_to_plane(point, Vector(rules.arena.width / 2, 0, 0), Vector(-1, 0, 0)));

    // Side z (goal)
    dan = min(dan, dan_to_plane(
        point,
        Vector(0, 0, (rules.arena.depth / 2) + rules.arena.goal_depth),
        Vector(0, 0, -1)));

    // Side z
    auto v = Vector(point.x, point.y, 0) - Vector(
        (rules.arena.goal_width / 2) - rules.arena.goal_top_radius,
        rules.arena.goal_height - rules.arena.goal_top_radius, 0);
    const auto lenCmpWith1 = rules.arena.goal_top_radius + rules.arena.goal_side_radius;
    if (point.x >= (rules.arena.goal_width / 2) + rules.arena.goal_side_radius
        || point.y >= rules.arena.goal_height + rules.arena.goal_side_radius
        || (
            v.x > 0
            && v.y > 0
            && (v).sqLen() >= lenCmpWith1 * lenCmpWith1)) {
        dan = min(dan, dan_to_plane(point, Vector(0, 0, rules.arena.depth / 2), Vector(0, 0, -1)));
    }
    // Side x & ceiling (goal)
    if (point.z >= (rules.arena.depth / 2) + rules.arena.goal_side_radius) {
        // x
        dan = min(dan, dan_to_plane(
            point,
            Vector(rules.arena.goal_width / 2, 0, 0),
            Vector(-1, 0, 0)));
        // y
        dan = min(dan, dan_to_plane(point, Vector(0, rules.arena.goal_height, 0), Vector(0, -1, 0)));
    }
    // Corner
    if (point.x > (rules.arena.width / 2) - rules.arena.corner_radius
        && point.z > (rules.arena.depth / 2) - rules.arena.corner_radius) {
        dan = min(dan, dan_to_sphere_inner(
            point,
            Vector(
            (rules.arena.width / 2) - rules.arena.corner_radius,
                point.y,
                (rules.arena.depth / 2) - rules.arena.corner_radius
            ),
            rules.arena.corner_radius));
    }
    // Goal outer corner
    if (point.z > rules.arena.depth / 2 - rules.BALL_RADIUS * 2
        && point.z < (rules.arena.depth / 2) + rules.arena.goal_side_radius) {
        // Side x
        if (point.x < (rules.arena.goal_width / 2) + rules.arena.goal_side_radius) {
            dan = min(dan, dan_to_sphere_outer(
                point,
                Vector(
                (rules.arena.goal_width / 2) + rules.arena.goal_side_radius,
                    point.y,
                    (rules.arena.depth / 2) + rules.arena.goal_side_radius
                ),
                rules.arena.goal_side_radius));
        }
        // Ceiling
        if (point.y < rules.arena.goal_height + rules.arena.goal_side_radius) {
            dan = min(dan, dan_to_sphere_outer(
                point,
                Vector(
                    point.x,
                    rules.arena.goal_height + rules.arena.goal_side_radius,
                    (rules.arena.depth / 2) + rules.arena.goal_side_radius
                ),
                rules.arena.goal_side_radius));
        }
        // Top corner
        auto o = Vector(
            (rules.arena.goal_width / 2) - rules.arena.goal_top_radius,
            rules.arena.goal_height - rules.arena.goal_top_radius,
            0
        );
        auto v = Vector(point.x, point.y, 0) - o;
        if (v.x > 0 && v.y > 0) {
            o += (v).norm() * (rules.arena.goal_top_radius + rules.arena.goal_side_radius);
            dan = min(dan, dan_to_sphere_outer(
                point,
                Vector(o.x, o.y, (rules.arena.depth / 2) + rules.arena.goal_side_radius),
                rules.arena.goal_side_radius));
        }
    }
    // Bottom corners
    if (point.y < rules.arena.bottom_radius) {
        // Side x
        if (point.x > (rules.arena.width / 2) - rules.arena.bottom_radius) {
            dan = min(dan, dan_to_sphere_inner(
                point,
                Vector(
                (rules.arena.width / 2) - rules.arena.bottom_radius,
                    rules.arena.bottom_radius,
                    point.z
                ),
                rules.arena.bottom_radius));
        }
        // Side z
        if (point.z > (rules.arena.depth / 2) - rules.arena.bottom_radius
            && point.x >= (rules.arena.goal_width / 2) + rules.arena.goal_side_radius) {
            dan = min(dan, dan_to_sphere_inner(
                point,
                Vector(
                    point.x,
                    rules.arena.bottom_radius,
                    (rules.arena.depth / 2) - rules.arena.bottom_radius
                ),
                rules.arena.bottom_radius));
        }
        if (point.z > rules.arena.depth / 2 - rules.BALL_RADIUS * 2) {
            // Goal outer corner
            auto o = Vector(
                (rules.arena.goal_width / 2) + rules.arena.goal_side_radius,
                (rules.arena.depth / 2) + rules.arena.goal_side_radius,
                0
            );
            auto v = Vector(point.x, point.z, 0) - o;
            const auto lenCmp2 = rules.arena.goal_side_radius + rules.arena.bottom_radius;
            if (v.x < 0 && v.y < 0
                && (v).sqLen() < lenCmp2 * lenCmp2) {
                o += (v).norm() * (rules.arena.goal_side_radius + rules.arena.bottom_radius);
                dan = min(dan, dan_to_sphere_inner(
                    point,
                    Vector(o.x, rules.arena.bottom_radius, o.y),
                    rules.arena.bottom_radius));
            }
        }
        // Side x (goal)
        if (point.z >= (rules.arena.depth / 2) + rules.arena.goal_side_radius
            && point.x > (rules.arena.goal_width / 2) - rules.arena.bottom_radius) {
            dan = min(dan, dan_to_sphere_inner(
                point,
                Vector(
                (rules.arena.goal_width / 2) - rules.arena.bottom_radius,
                    rules.arena.bottom_radius,
                    point.z
                ),
                rules.arena.bottom_radius));
        }
        // Corner
        if (point.x > (rules.arena.width / 2) - rules.arena.corner_radius
            && point.z > (rules.arena.depth / 2) - rules.arena.corner_radius) {
            auto corner_o = Vector(
                (rules.arena.width / 2) - rules.arena.corner_radius,
                (rules.arena.depth / 2) - rules.arena.corner_radius,
                0
            );
            auto n = Vector(point.x, point.z, 0) - corner_o;
            auto dist = n.sqLen();
            const auto lenCmp3 = rules.arena.corner_radius - rules.arena.bottom_radius;
            if (dist > lenCmp3 * lenCmp3) {
                n /= sqrt(dist);
                auto o2 = corner_o + n * (rules.arena.corner_radius - rules.arena.bottom_radius);
                dan = min(dan, dan_to_sphere_inner(
                    point,
                    Vector(o2.x, rules.arena.bottom_radius, o2.y),
                    rules.arena.bottom_radius));
            }
        }
    }
    // Ceiling corners
    if (point.y > rules.arena.height - rules.arena.top_radius) {
        // Side x
        if (point.x > (rules.arena.width / 2) - rules.arena.top_radius) {
            dan = min(dan, dan_to_sphere_inner(
                point,
                Vector(
                (rules.arena.width / 2) - rules.arena.top_radius,
                    rules.arena.height - rules.arena.top_radius,
                    point.z
                ),
                rules.arena.top_radius));
        }
        // Side z
        if (point.z > (rules.arena.depth / 2) - rules.arena.top_radius) {
            dan = min(dan, dan_to_sphere_inner(
                point,
                Vector(
                    point.x,
                    rules.arena.height - rules.arena.top_radius,
                    (rules.arena.depth / 2) - rules.arena.top_radius
                ),
                rules.arena.top_radius));
        }
        // Corner
        if (point.x > (rules.arena.width / 2) - rules.arena.corner_radius
            && point.z > (rules.arena.depth / 2) - rules.arena.corner_radius) {
            auto corner_o = Vector(
                (rules.arena.width / 2) - rules.arena.corner_radius,
                (rules.arena.depth / 2) - rules.arena.corner_radius,
                0
            );
            auto dv = Vector(point.x, point.z, 0) - corner_o;
            const auto lenCmp4 = rules.arena.corner_radius - rules.arena.top_radius;
            if ((dv).sqLen() > lenCmp4 * lenCmp4) {
                auto n = (dv).norm();
                auto o2 = corner_o + n * (rules.arena.corner_radius - rules.arena.top_radius);
                dan = min(dan, dan_to_sphere_inner(
                    point,
                    Vector(o2.x, rules.arena.height - rules.arena.top_radius, o2.y),
                    rules.arena.top_radius));
            }
        }
    }
    return dan;
}

std::pair<double, Vector> MyWorldSim::dan_to_arena(Vector point) {
    auto negate_x = point.x < 0;
    auto negate_z = point.z < 0;
    if (negate_x) {
        point.x = -point.x;
    }
    if (negate_z) {
        point.z = -point.z;
    }
    auto result = dan_to_arena_quarter(point);
    if (negate_x) {
        result.second.x = -result.second.x;
    }
    if (negate_z) {
        result.second.z = -result.second.z;
    }
    return result;
}