#if defined(_MSC_VER) && (_MSC_VER >= 1200)
#pragma once
#endif
#include "MyVisHelper.h"

#ifndef _MY_STRATEGY_H_
#define _MY_STRATEGY_H_

#include "Strategy.h"
#include <string>

class MyStrategy : public Strategy {
public:
    MyStrategy();

    void act(const model::Robot& me, const model::Rules& rules, const model::Game& world, model::Action& action) override;
	std::string custom_rendering() override;
private:
	MyVisHelper wantRender;
    int lastTick = -1;
};

#endif
