﻿#include "MyAttacker.h"
#include "MyMath.h"
#include "MyWorldSim.h"
#include "MyHelper.h"
#include "MyRobotRoleDeterminer.h"
#include "MyTimeManager.h"

MyAttacker::MyAttacker(const Robot & me, const Rules & rules, const Game & game, Action & action)
    : MyActionWrapper(me, rules, game, action) {
}

void MyAttacker::act() {
    if (!me.touch) {
        nitroTrick();
        return;
    }

    double nestedPrecision = 0.5;
    int ticksToSim = 200;
    int tickToNestedSim = 45;
    if (game.robots.size() == 6) {
        ticksToSim = 160;
        tickToNestedSim = 30;
        nestedPrecision = 0.3;
    }
    if (MyTimeManager::getMs() > 300000) {
        tickToNestedSim -= 5;
        nestedPrecision -= 0.1;
        ticksToSim -= 10;
    }
    if (MyTimeManager::getMs() > 340000) {
        tickToNestedSim -= 6;
        nestedPrecision -= 0.15;
        ticksToSim -= 10;
    }

    const auto interceptY = rules.arena.goal_height - 1;

    const auto gatesXL = -rules.arena.goal_width / 2;
    const auto gatesXR = rules.arena.goal_width / 2;
    const auto enemyGatesPt = Vector(0, 0, rules.arena.depth / 2 + 9);
    const auto ourGatesPt = Vector(0, 0, -rules.arena.depth / 2 - 7);
    const auto phyBall = MyPhyBall(game.ball, rules);
    const auto enemies = Helper::getEnemyRobotsPhy(game, rules);
    auto robotsToSim = enemies;
    for (auto& x : game.robots) {
        if (x.is_teammate && x.id != me.id) {
            robotsToSim.emplace_back(x, rules, MyPhyRobotAction::empty);
            robotsToSim.back().is_teammate = false; // sorry, bro
        }
    }
    const auto srcSima = MyWorldSim(phyBall, robotsToSim, rules);
    auto finalBallSima = srcSima;
    finalBallSima.setPrecision(nestedPrecision);
    if (thisPhyRobotStill.velocity.len() < 2) {
        finalBallSima.getRobots().push_back(thisPhyRobotStill);
    }
    for (int i = 0; i < tickToNestedSim; ++i) {
        finalBallSima.tick();
        if (finalBallSima.getBall().position.z < -rules.arena.depth / 2 + rules.BALL_RADIUS) {
            finalBallSima.getBall().position.z = -rules.arena.depth / 2 - rules.arena.goal_depth;
            break;
        }
        if (finalBallSima.getBall().position.z > rules.arena.depth / 2 + rules.BALL_RADIUS) {
            finalBallSima.getBall().position.z = rules.arena.depth / 2 + rules.arena.goal_depth;
            break;
        }
    }

    auto sima = srcSima;
    if (thisPhyRobotStill.velocity.len() < 2) {
        sima.getRobots().push_back(thisPhyRobotStill);
    }
    sima.setPrecision(0.35);

    Vector landingPt = phyBall.position;
    int landingTick = 0;
    Vector headPt = phyBall.position;
    int headTick = 0;
    Vector outOfCorner = phyBall.position;
    int outOfCornerTick = 0;
    const Vector cornerVector = Vector(rules.arena.width / 2, 0, rules.arena.depth / 2);
    const auto isInCorner = [&](const MyPhyBall& phyBall) {
        return phyBall.position.absx().dist2dTo(cornerVector) + rules.BALL_RADIUS / 2 < rules.arena.corner_radius;
    };
    bool getOutOfCorner = isInCorner(phyBall);

    double discount = 0.91;
    std::vector<InterceptionCandidate> candidates;
    std::vector<InterceptionResult> results;
    {
        for (int i = 0; i < ticksToSim; ++i) {
            if (sima.getBall().position.y <= interceptY && i < tickToNestedSim - 3) {
                double angleToUse;
                if (sima.getBall().position.z < 0) {
                    const auto angleBallToAlly = sima.getBall().position.angle2DTo(ourGatesPt);
                    angleToUse = angleBallToAlly; // def mode
                }
                else {
                    const auto angleBallToGates = enemyGatesPt.angle2DTo(sima.getBall().position);
                    angleToUse = angleBallToGates; // atck mode
                }
                const auto robotRadius = rules.ROBOT_RADIUS;
                const auto angleToUseCos = cos(angleToUse);
                const auto angleToUseSin = sin(angleToUse);
                JumpTargetAlgorithm interceptAlgo = [=](const Vector& ballPos) {
                    return Vector(ballPos.x + robotRadius * angleToUseCos,
                        ballPos.y,
                        ballPos.z + robotRadius * angleToUseSin);
                };

                const auto interceptPos = interceptAlgo(sima.getBall().position);
                {
                    candidates.emplace_back(interceptPos, sima.getBall().position, rules.ROBOT_MAX_JUMP_SPEED, interceptAlgo, i);
                }
                if (sima.getBall().position.z > 16) {
                    candidates.emplace_back(interceptPos, sima.getBall().position, rules.ROBOT_MAX_JUMP_SPEED / 1.8, interceptAlgo, i);
                }
                else if (sima.getBall().position.z < -12 && sima.getBall().position.y > rules.BALL_RADIUS + 0.5) {
                    const auto ballRadius = rules.ROBOT_RADIUS;
                    const JumpTargetAlgorithm algo = [=](const Vector& ballPos) {
                        return Vector(ballPos.x + ballRadius * angleToUseCos * 0.8,
                            ballPos.y - ballRadius * 0.7,
                            ballPos.z + ballRadius * angleToUseSin * 0.8);
                    };
                    candidates.emplace_back(algo(sima.getBall().position), sima.getBall().position, rules.ROBOT_MAX_JUMP_SPEED, algo, i);
                }
            }

            sima.tick();

            if (sima.getBall().velocity.y < 0 && sima.getBall().position.y < rules.BALL_RADIUS * 1.2 && landingTick == 0) {
                landingPt = sima.getBall().position;
                landingTick = i;
            }

            if (sima.getBall().velocity.y < 0
                && sima.getBall().position.y > rules.BALL_RADIUS + rules.ROBOT_RADIUS
                && sima.getBall().position.y < rules.BALL_RADIUS + rules.ROBOT_RADIUS + 0.5
                && headTick == 0) {
                headPt = sima.getBall().position.noY();
                headTick = i;
            }

            if (getOutOfCorner && !isInCorner(sima.getBall())) {
                outOfCorner = sima.getBall().position;
                getOutOfCorner = false;
                outOfCornerTick = i;
            }
        }

        if (game.ball.velocity_z > 0 && phyBall.position.noY().dist2dTo(landingPt) > 3) { // replace landing pt with head pt
            landingPt = headPt;
            landingTick = headTick;
            const auto angleToBall = headPt.angle2DTo(phyBall.position);
            landingPt.x += cos(angleToBall);
            landingPt.z += sin(angleToBall);
        }

        if (outOfCornerTick > landingTick) {
            landingPt = outOfCorner;
            landingTick = outOfCornerTick;
        }

        if (game.ball.x <= gatesXR && game.ball.x >= gatesXL &&
            (game.ball.z >= rules.arena.depth / 2 - rules.arena.goal_depth / 2 || game.ball.z <= -rules.arena.depth / 2 + rules.arena.goal_depth / 2)) {
            landingPt = phyBall.position.noY();
            if (me.velocity_z > 15 && game.ball.z > 0) {
                waitDistAdd = 2;
            }
        }
    }

    const RepositionFunction currentRepositionFunction = [&](const Vector& robotPos) {
        if (landingPt == headPt) {
            return headPt;
        }
        return getReposPoint(landingPt, robotPos);
    };

    candidates.emplace_back(thisPhyRobotStill.position + Vector(0, 0.1, 0), phyBall.position, rules.ROBOT_MAX_JUMP_SPEED, [=](const Vector& ballPos) { return ballPos; }, 1);

    for (auto& x : candidates) {
        auto interceptionResult = interceptScore(srcSima, x.ticksPassed, x.interceptPoint, x.jumpSpeed, [&](const MyPhyBall& resultBall, const MyPhyRobot&) {
            int score = 100 * (resultBall.position.z - finalBallSima.getBall().position.z);
            score += 50 * (resultBall.velocity.z - finalBallSima.getBall().velocity.z);
            score -= 10 * (abs(resultBall.position.x) - abs(finalBallSima.getBall().position.x));
            return score;
        }, tickToNestedSim, nestedPrecision, currentRepositionFunction, x.algo);
        interceptionResult.ballPos = x.ballPos;
        interceptionResult.ticksPassed = x.ticksPassed;
        if (interceptionResult.score >= 0) {
            results.push_back(interceptionResult);
        }
    }

    if (!results.empty()) {
        std::vector<double> precompDiscount;
        double disc = 1;
        for (int i = 0; i < tickToNestedSim; ++i) {
            precompDiscount.push_back(disc);
            disc *= discount;
        }
        const auto maxInterception = *std::max_element(results.begin(), results.end(), [&](const InterceptionResult& a, const InterceptionResult& b) {
            return a.score * precompDiscount[a.ticksPassed] < b.score * precompDiscount[b.ticksPassed];
        });
        if (maxInterception.timeDiff < 1) {
            runJumpTo(maxInterception.point, maxInterception.algo, maxInterception.jumpSpeed);
#ifdef LOCALRUN
            visio.drawSphere(MyVisSphere(maxInterception.point, 1.2, Vector(1, 0, 0)));
            visio.drawSphere(MyVisSphere(maxInterception.ballPos, 2, Vector(1, 1, 0)));
            visio.drawSphere(MyVisSphere(maxInterception.interBallPos, 2, Vector(0, 1, 0)));
            visio.drawSphere(MyVisSphere(finalBallSima.getBall().position, 2, Vector(0, 0, 1)));
            visio.setText(std::to_string(maxInterception.timeDiff) + ":" + std::to_string(maxInterception.score));
#endif
            return;
        }
    }

    // goto wait point
    runJumpTo(currentRepositionFunction(thisPhyRobotStill.position));
}

Vector MyAttacker::getReposPoint(const Vector& landingPt, const Vector& robotPosition) {
    const auto enemyGatesPt = Vector(0, 0, rules.arena.depth / 2 + 7);

    int waitDistance = 3;
    waitDistance += waitDistAdd;
    if (!mainInThisRole) {
        waitDistance = 5;
    }

#ifdef LOCALRUN
    visio.drawSphere(MyVisSphere(landingPt, 2, Vector(1, 1, 1)));
#endif

    const auto angleBallToGates = landingPt.angle2DTo(enemyGatesPt);
    auto backPt = Vector(landingPt.x - cos(angleBallToGates) * rules.ROBOT_RADIUS * waitDistance,
        0,
        landingPt.z - sin(angleBallToGates) * rules.ROBOT_RADIUS * waitDistance);
    if (backPt.z > robotPosition.z || game.ball.y > rules.ROBOT_RADIUS * 3) {
        backPt.z = std::min(backPt.z, rules.arena.depth / 2 - 1);
        return backPt;
    }

    const auto angleToRobot = landingPt.angle2DTo(robotPosition);
    const auto p1 = Vector(landingPt.x + cos(angleToRobot + _PI / 4) * rules.ROBOT_RADIUS * 4,
        0,
        landingPt.z + sin(angleToRobot + _PI / 4) * rules.ROBOT_RADIUS * 4);
    const auto p2 = Vector(landingPt.x + cos(angleToRobot - _PI / 4) * rules.ROBOT_RADIUS * 4,
        0,
        landingPt.z + sin(angleToRobot - _PI / 4) * rules.ROBOT_RADIUS * 4);
    if (backPt.dist2dTo(p1) < backPt.dist2dTo(p2)) {
        return p1;
    }
    return p2;
}
