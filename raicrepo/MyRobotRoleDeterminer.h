﻿#pragma once

#include "Strategy.h"
#include <map>
using namespace model;

enum class RobotRole {
	ATTACKER,
	DEFENDER
};

class MyRobotRoleDeterminer {
public:
	static RobotRole determine(const Robot & me, const Rules & rules, const Game & game);
    static std::pair<int, RobotRole> whoAmI(int id);
    static void clear();
	MyRobotRoleDeterminer() = delete;
private:
    static int attackers;
    static int defenders;
    static std::map<int, std::pair<int, RobotRole>> idToRole;
};
