﻿#pragma once

#include <functional>
#include "Strategy.h"
#include "MyMath.h"
#include "MyVisHelper.h"
#include "MyWorldSim.h"
#include "MyPersistence.h"
using namespace model;

using RepositionFunction = std::function<Vector(const Vector&)>;

struct InterceptionResult {
    InterceptionResult(int score, int timeDiff, const Vector& pt, int jumpSpeed, JumpTargetAlgorithm algo = JumpTargetAlgorithm())
        : score(score)
        , timeDiff(timeDiff)
        , jumpSpeed(jumpSpeed)
        , algo(algo)
        , point(pt) {
    }

    int ticksPassed;
    int score, timeDiff;
    int jumpSpeed;
    JumpTargetAlgorithm algo;
    Vector point;
    Vector ballPos;
    Vector interBallPos;
};

struct InterceptionCandidate {
    InterceptionCandidate(const Vector& point, const Vector& ballPos, double jumpSpeed, JumpTargetAlgorithm algo,
        int ticksPassed)
        : interceptPoint(point)
        , ballPos(ballPos)
        , jumpSpeed(jumpSpeed)
        , algo(algo)
        , ticksPassed(ticksPassed) {
    }

    Vector interceptPoint;
    Vector ballPos;
    double jumpSpeed;
    JumpTargetAlgorithm algo;
    int ticksPassed;
};

class MyActionWrapper {
public:
    MyActionWrapper(const Robot& me, const Rules& rules, const Game& game, Action& action);

    virtual void act() = 0;
    void runJumpTo(const Vector& target, JumpTargetAlgorithm targetAlgo = [](const Vector&) {return Vector::zero; }, double jumpspeed = -1) const;

    void nitroTrick();

    MyVisHelper wantVis() const;

    virtual ~MyActionWrapper() {};
protected:
    InterceptionResult interceptScore(MyWorldSim sima, int ticksPassed,
                                      const Vector& originalTarget, double jumpSpeed,
                                      const std::function<int(const MyPhyBall&, const MyPhyRobot&)>& valueFunc, int ticksToSim, double simaPrecision,
                                      RepositionFunction respositionFunction, JumpTargetAlgorithm jumpTargetAlgorithm) const;

    const Robot& me;
    const Rules& rules;
    const Game& game;
    const int inRoleNumber;
    const bool mainInThisRole;
    const MyPhyRobot thisPhyRobotStill;
    Action& action;

    MyVisHelper visio;
private:
    Vector nitroVelo(const MyPhyBall& phyBall, const MyPhyRobot& robot) const;
};
