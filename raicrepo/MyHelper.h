#pragma once

#include "MyStrategy.h"
#include <vector>
#include "MyWorldSim.h"
using namespace model;

class Helper {
public:
	static std::vector<MyPhyRobot> getEnemyRobotsPhy(const Game& game, const Rules& rules) {
		auto ret = std::vector<MyPhyRobot>();
		for (const auto& r : game.robots) {
			if (!r.is_teammate) {
				ret.emplace_back(r, rules, MyPhyRobotAction::empty);
			}
		}
		return ret;
	}
};