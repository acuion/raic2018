#include "MyVisHelper.h"



MyVisHelper::MyVisHelper()
{
}


MyVisHelper::~MyVisHelper()
{
}

void MyVisHelper::drawSphere(const MyVisSphere & sphere) {
	spheres.push_back(sphere);
}

void MyVisHelper::setText(const std::string& text) {
    this->text += text;
}

void MyVisHelper::reset() {
	spheres.clear();
    text = "";
}

void MyVisHelper::add(const MyVisHelper& visio) {
    for (auto& x :visio.spheres) {
        spheres.push_back(x);
    }
    text += visio.text;
}

std::string MyVisHelper::asJson() const {
	std::string res = "[";
    res += "{\"Text\": \"" + text + "\"},";
	for (auto& x : spheres) {
		res += "{\"Sphere\": {";
		res += "\"x\": " + std::to_string(x.center.x) + ",";
		res += "\"y\": " + std::to_string(x.center.y) + ",";
		res += "\"z\": " + std::to_string(x.center.z) + ",";
		res += "\"radius\": " + std::to_string(x.radius) + ",";
		res += "\"r\": " + std::to_string(x.color.x) + ",";
		res += "\"g\": " + std::to_string(x.color.y) + ",";
		res += "\"b\": " + std::to_string(x.color.z) + ",";
		res += "\"a\": 0.7";
		res += "}},";
	}
	if (res.size() > 1) {
		res.pop_back();
	}
	res += "]";
	return res;
}

MyVisSphere::MyVisSphere(Vector center, double radius, Vector color)
	: center(center)
	, radius(radius)
	, color(color) {
}
