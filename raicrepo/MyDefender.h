﻿#pragma once

#include "MyActionWrapper.h"
#include "MyWorldSim.h"
using namespace model;

class MyDefender : public MyActionWrapper {
public:
	MyDefender(const Robot& me, const Rules& rules, const Game& game, Action& action);

	void act() override;
    Vector getReposPoint(const Vector& currentBallPos, const Vector& futureBallPos, const Vector& robotPosition);
    Vector getReposPointAtck(const Vector& currentBallPos, const Vector& futureBallPos, const Vector& robotPosition);
};
