#pragma once

#include "MyMath.h"
#include <string>
#include <vector>

struct MyVisSphere {
	MyVisSphere(Vector center, double radius, Vector color);

	Vector center;
	double radius;
	Vector color;
};

class MyVisHelper
{
public:
	MyVisHelper();
	~MyVisHelper();

	void drawSphere(const MyVisSphere& sphere);
    void setText(const std::string& text);
	void reset();
    void add(const MyVisHelper& visio);
	std::string asJson() const;
private:
    std::string text;
	std::vector<MyVisSphere> spheres;
};

