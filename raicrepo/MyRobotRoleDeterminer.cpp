﻿#include "MyRobotRoleDeterminer.h"
#include "MyMath.h"
#include <iostream>
#include "MyWorldSim.h"
#include "MyHelper.h"

int MyRobotRoleDeterminer::attackers;
int MyRobotRoleDeterminer::defenders;
std::map<int, std::pair<int, RobotRole>> MyRobotRoleDeterminer::idToRole;

RobotRole MyRobotRoleDeterminer::determine(const Robot & me, const Rules & rules, const Game & game) {
	const Robot* defender = &me;
	for (auto& x : game.robots) {
		if (x.is_teammate) {
			if (!Vector(*defender).lessThanZxy(Vector(x))) {
				defender = &x;
			}
		}
	}

    if (defender == &me) {
        idToRole[me.id] = { defenders, RobotRole::DEFENDER };
        ++defenders;
        return RobotRole::DEFENDER;
    }
    else {
        if (attackers == 1 && game.ball.z < -30) {
            idToRole[me.id] = { defenders, RobotRole::DEFENDER };
            ++defenders;
            return RobotRole::DEFENDER;
        }
        idToRole[me.id] = { attackers, RobotRole::ATTACKER };
        ++attackers;
        return RobotRole::ATTACKER;
    }
}

std::pair<int, RobotRole> MyRobotRoleDeterminer::whoAmI(int id) {
    return idToRole[id];
}

void MyRobotRoleDeterminer::clear() {
    attackers = 0;
    defenders = 0;
    idToRole.clear();
}
