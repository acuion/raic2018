#pragma once

class MyTimeManager {
public:
    MyTimeManager() = delete;

    static void start();
    static void stop();
    static long long getMs();
private:
    static long long now();
    static long long startMs;
    static long long msPassed;
};