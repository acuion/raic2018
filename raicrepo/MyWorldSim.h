#pragma once
#include "Strategy.h"
#include <vector>
#include "MyMath.h"
using namespace model;

class MyPhyObject {
public:
    MyPhyObject(const Robot& robot, const Rules& rules, int id);
    MyPhyObject(const Ball& ball, const Rules& rules);

    Vector velocity;
    Vector position;
    double mass;
    double invMass;
    double radius;
    double radius_change_speed;
	double arena_e;
	int id;
	bool collidedWithBallThisTurn;
protected:
    const Rules& rules;
};

struct MyPhyRobotAction {
    MyPhyRobotAction(Vector targetVelocity, double targetJumpSpeed, bool use_nitro = false);
	MyPhyRobotAction(const Action& action);

    static MyPhyRobotAction empty;

    Vector target_velocity;
	double jump_speed;
    bool use_nitro;
};

class MyPhyRobot : public MyPhyObject {
public:
    MyPhyRobot(const Robot& robot, const Rules& rules, MyPhyRobotAction action);

    bool jumpWillHitInTicks(const Vector& target, double jumpspeed, double nitoAmount) const;

    bool touch;
    Vector touch_normal;
    double radius;
	bool is_teammate;
    double nitro;

    MyPhyRobotAction action;
};

class MyPhyBall : public MyPhyObject {
public:
    MyPhyBall(const Ball& ball, const Rules& rules);
};

class MyWorldSim {
public:
	MyWorldSim(const MyPhyBall& ball,
        const std::vector<MyPhyRobot>& robots,
        const Rules& rules,
		bool ballEnabled = true);

	void step(double delta);
	void tick();

	MyPhyBall& getBall();
    std::vector<MyPhyRobot>& getRobots();
	void setPrecision(double p);

private:
	Vector collide_with_arena(MyPhyObject & e);
    void move(MyPhyObject& e, double delta_time);
    void collide_entities(MyPhyObject& a, MyPhyObject& b);

    std::pair<double, Vector> dan_to_plane(const Vector& point, const Vector& point_on_plane, const Vector& plane_normal);
    std::pair<double, Vector> dan_to_sphere_inner(const Vector& point, const Vector& sphere_center, double sphere_radius);
    std::pair<double, Vector> dan_to_sphere_outer(const Vector& point, const Vector& sphere_center, double sphere_radius);
	std::pair<double, Vector> dan_to_arena_quarter(const Vector& point);
	std::pair<double, Vector> dan_to_arena(Vector point);

    const Rules& rules;

	bool ballEnabled;
    MyPhyBall ball;
	std::vector<MyPhyRobot> robots;

	int useMicroticks;
};