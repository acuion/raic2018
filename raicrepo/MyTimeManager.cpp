#include "MyTimeManager.h"
#include <chrono>

using namespace std::chrono;

long long MyTimeManager::msPassed = 0;
long long MyTimeManager::startMs = 0;

void MyTimeManager::start() {
    startMs = now();
}

void MyTimeManager::stop() {
    msPassed += now() - startMs;
}

long long MyTimeManager::getMs() {
    return msPassed;
}

long long MyTimeManager::now() {
    return duration_cast<milliseconds>(system_clock::now().time_since_epoch()).count();
}
